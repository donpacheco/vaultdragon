from django.conf.urls import url

from rest_framework.urlpatterns import format_suffix_patterns

from keystore.views import ObjectApi

urlpatterns = format_suffix_patterns([

    url(r'^$', ObjectApi.as_view(), name='object-api'),
    url(r'^(?P<key>\w+)/$', ObjectApi.as_view(), name='object-api'),

])
