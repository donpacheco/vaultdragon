from __future__ import unicode_literals

from django.apps import AppConfig


class KeystoreConfig(AppConfig):
    name = 'keystore'
