import datetime
import json

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser

from models import Storage
from serializers import StorageSerializer


class ObjectApi(APIView):
    """
    API for Submitting and recording of versioned Objects
    Request types: GET and POST

    Headers: None

    ## POST
    Payload:
    {
        "key" : "value"
    }
    """
    __name__ = 'ObjectApi'

    def get(self, request, key=None, format=None):
        """
        :param request:
        :param key:
        :return:
        """
        timestamp = request.GET.get('timestamp', '')

        try:
            objs = Storage.objects.filter(key=key)
        except Storage.DoesNotExist:
            # No objects exist!
            return Response({}, status=status.HTTP_404_NOT_FOUND)

        if timestamp:
            # Filter objects with timestamp
            try:
                timestamp = datetime.datetime.fromtimestamp(float(timestamp))
            except ValueError:
                return Response({"message": "Invalid Timestamp"}, status=status.HTTP_400_BAD_REQUEST)
            objs = objs.filter(date_created__lte=timestamp)

        if objs.count() >= 1:
            # Get latest object from list
            data = objs[0].value

        else:
            # Objects filtered out by timestamp
            return Response({}, status=status.HTTP_404_NOT_FOUND)

        return Response(data, status=status.HTTP_200_OK)

    def post(self, request, format=None):
        """
        :param request:
        :return:
        """

        try:
            data = request.data
        except ValueError:
            return Response({"message" : "Invalid JSON"}, status=status.HTTP_400_BAD_REQUEST)


        for obj in data:
            serializer = StorageSerializer(data={'key': obj, 'value': data[obj]})
            if serializer.is_valid():
                serializer.save()
                return Response({}, status=201)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

