from __future__ import unicode_literals

from django.db import models


class Storage(models.Model):
    key = models.CharField(max_length=50, default="")
    value = models.TextField(blank=True, null=True)
    date_created = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-date_created']
