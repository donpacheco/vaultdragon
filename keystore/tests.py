import time
import json

from django.test import TestCase
from django.core.urlresolvers import reverse

from models import Storage

# Create your tests here.
class StorageTestCase(TestCase):
    def setUp(self):
        Storage.objects.create(key="mykey", value="value1")
        Storage.objects.create(key="mykey", value="value2")
        time.sleep(1)
        self.epoch = time.time().__int__()
        print self.epoch
        time.sleep(1)
        Storage.objects.create(key="mykey", value="value3")
        Storage.objects.create(key="mykey", value="value4")
        self.create_read_url = reverse("object-api")

    def test_read(self):
        response = self.client.get(reverse("object-api", kwargs={'key':"mykey"}))
        self.assertContains(response, "value4")

    def test_read_latest(self):
        post = json.dumps({'mykey': 'value5'})
        response = self.client.post(self.create_read_url, post, "application/json")
        self.assertEquals(response.status_code, 201)
        response = self.client.get(reverse("object-api", kwargs={'key':"mykey"}))
        self.assertContains(response, "value5")


    def test_read_timestamp(self):
        response = self.client.get(reverse("object-api", kwargs={'key':"mykey"}) + '?timestamp='+str(self.epoch))
        self.assertContains(response, "value2")



