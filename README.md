#vaultdragon

# Prerequisites
- [virtualenv](https://virtualenv.pypa.io/en/latest/)
- [postgresql](http://www.postgresql.org/)

# Initialize the project
Create and activate a virtualenv:

```bash
virtualenv env
source env/bin/activate
```
Install dependencies:

```bash
pip install -r requirements/local.txt
```
Create the database:

```bash
createdb vaultdragon
```
Initialize the git repository

```
git init
git remote add origin git@github.com:donpacheco/vaultdragon.git
```

Migrate the database and create a superuser:
```bash
python vaultdragon/manage.py migrate
python vaultdragon/manage.py createsuperuser
```

Run the development server:
```bash
python vaultdragon/manage.py runserver
```
